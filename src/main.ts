import { createApp } from "vue";
import "./styles.css";
import App from "./App.vue";
import * as VueRouter from "vue-router";
import Login from "./views/Login.vue";
import Home from "./views/Home.vue";
import Details from "./views/Details.vue";

const routes = [
  { path: "/", component: Login },
  { path: "/home", name: "home", component: Home },
  { path: "/details", name: "details", component: Details },
];

const router = VueRouter.createRouter({
  history: VueRouter.createWebHistory(),
  routes,
});

createApp(App).use(router).mount("#app");
