// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

use std::fs::File;
use std::io::{self, Write};

#[tauri::command]
fn write_beer_to_file() {
    println!("I was invoked from JS!");
}

fn main() {
    tauri::Builder::default()
        .invoke_handler(tauri::generate_handler![write_beer_to_file])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
